<?php
if(!isset($_SESSION)) { session_start(); }

class Engine {
    function __construct(){
        $this->player = new Player();
        $this->dealer = new Dealer();
        $this->message = "";
        $this->buttonState = "visible";
        $this->initGame();
    }

    function initGame(){
        $this->dealer->shuffleDeck();
        
        $this->dealer->dealCard($this->player);
        $this->dealer->dealCard($this->player);

        $this->dealer->dealCard($this->dealer);
        $this->dealer->dealCard($this->dealer);

        if($this->dealer->evaluateFirstTurn($this->player) !== ""){
            $this->message = $this->dealer->evaluateFirstTurn($this->player);
            $this->buttonState = "hidden";
        }
    }
}
?>