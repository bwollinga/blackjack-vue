const title       = document.getElementById("title");
const dealername  = document.getElementById("dealername");
const dealerhand  = document.getElementById("dealerhand");
const playername  = document.getElementById("playername");
const playerhand  = document.getElementById("playerhand");
const playerscore = document.getElementById("playerscore");
const hitbutton   = document.getElementById("hit");
const standbutton = document.getElementById("stand");
const message     = document.getElementById("message");
const resetbutton = document.getElementById("newGame")

function initGui(){
  title.innerHTML       = "<h1>Blackjack</h1>";
  dealername.innerHTML  = "<h2>Dealer:</h2>";
  playername.innerHTML  = "<h2>Speler:</h2>";
  hitbutton.innerHTML   = '<input type="button" value="Hit" id="hit" onclick="button(\'/hit\');">';
  standbutton.innerHTML = '<input type="button" value="Stand" id="stand" onclick="button(\'/stand\');">';
  resetbutton.innerHTML = '<input type="button" value="New Game" id="newGame" onclick="button(\'/newgame\');">';
}

function renderTable() { 
  let xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      let response = JSON.parse(this.responseText);

      hitbutton.style.visibility = response["buttonState"];
      standbutton.style.visibility = response["buttonState"];
      playerscore.innerHTML = "score:"+response["scores"]["playerscore"];
      dealerscore.innerHTML = "score:"+response["scores"]["dealerscore"];
      message.innerHTML = response["message"];
      
      renderCards(response["hands"]);
    }
  }
  let route = '/get_gamestate';
  xhttp.open('GET', route);
  xhttp.send();
}

function renderCards(handsarray){
  playerhand.innerHTML = '';
  dealerhand.innerHTML = '';

  for (hand in handsarray){
      for(cards in handsarray[hand]){
        let src = document.getElementById(hand);
        let img = document.createElement("img");
        img.src = "/renderer/cardimages/" + handsarray[hand][cards]['filename'];
        src.appendChild(img);
      }
  }
}

function button(route){
  var myRequest  = new XMLHttpRequest();
  myRequest.open('GET', route);
  myRequest.send();
  renderTable();
}

