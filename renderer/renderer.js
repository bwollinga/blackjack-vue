var table = new Vue({
  el: '#table',
  data: {
    show: true,
    json: "",
  },
  methods: {
    renderTable: function() { 

      let xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          let response = JSON.parse(this.responseText);
          table.json = response;
        }
      }
      let route = '/get_gamestate';
      xhttp.open('GET', route);
      xhttp.send();

    }
  }
})

var dealer = new Vue({
  el: '#dealer',
  data: {
    show: true,
    items: []
  },
  methods: {

  }
})

var player = new Vue({
  el: '#player',
  data: {
    show: true,
    items: []
  },
  methods: {

  }
})

